package com.accenture.banco.controller;

import org.apache.camel.CamelContext;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.banco.model.Cuenta;
import com.accenture.banco.repository.CuentaDao;

@RestController
@RequestMapping({"/banco"})
public class MainController {
	
	@Autowired
	private CuentaDao cuentadao;
	
	@Autowired
	private CamelContext context;
	
	@GetMapping(path={"/test"})
	public ResponseEntity<Object> test() {
		
		JSONObject obj = new JSONObject();
		
		obj.put("error", 0);
		obj.put("message", "un test");
		
		return ResponseEntity.ok().body(obj.toString());
	}
	
	@PostMapping(path={"/crear"})
	public Cuenta createCuenta(@RequestBody Cuenta cuenta) {
	
		return cuentadao.save(cuenta);
	}
	
	//falta un if que separe error de exito
	@DeleteMapping(path= {"/baja/{id}"})
	public ResponseEntity<Object> Eliminar(@PathVariable Long id) {
		
		cuentadao.deleteById(id);
		
		JSONObject obj = new JSONObject();
		
		obj.put("error", 0);
		obj.put("message", "cuenta eliminada");
		
		return ResponseEntity.ok().body(obj.toString());
	}
	
	//falta agregar if en caso de error
	@PutMapping(path = {"/depositar/{id}/{saldo}"})
	public ResponseEntity<Object> Depositar(@PathVariable Long id, @PathVariable double saldo) {
		
		Cuenta cuenta1 = cuentadao.findById(id).orElse(null);
		
		double saldoactual = cuenta1.getSaldo();
		
		if(cuenta1 != null) {
			
			cuenta1.setSaldo(saldoactual + saldo);
			
			cuentadao.save(cuenta1);
			
		}
		
		JSONObject obj = new JSONObject();
		
		obj.put("error", 0);
		obj.put("saldo viejo", saldoactual);
		obj.put("saldo nuevo", cuenta1.getSaldo());
		
		return ResponseEntity.ok().body(obj.toString());
	}
	
	//falta agregar if en el caso de error
	@PutMapping(path = {"/extraer/{id}"})
	public ResponseEntity<Object> Extraer(@PathVariable Long id, @RequestBody Cuenta mock) {
		
		Cuenta cuenta1 = cuentadao.findById(id).orElse(null);
		
		double saldoactual = cuenta1.getSaldo();
		
		if(cuenta1 != null) {
			
			cuenta1.setSaldo(saldoactual - mock.getSaldo());
		}
		
		JSONObject obj = new JSONObject();
		
		obj.put("error", 0);
		obj.put("saldo viejo", saldoactual);
		obj.put("saldo nuevo", cuenta1.getSaldo());
		
		return ResponseEntity.ok().body(obj.toString());
	}

	
}
