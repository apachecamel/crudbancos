package com.accenture.banco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.banco.model.Cuenta;

public interface CuentaDao extends JpaRepository<Cuenta, Long> {
	
	public List<Cuenta> findByNombre(String nombre);

}
